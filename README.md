# Mine Hack

A HJSON mod for Mindustry.

[[New "Bug" Issue](https://gitlab.com/ApolloX939/mine-hack/-/issues/new?issuable_template=bug)]

This mod tries to maintain a sense of semi balance while providing a advantage over vanilla Mindustry.

To begin, start doing silicon production.

## Currently working on

- Telepod has 2 major graphical issues: (Both of these issues are not desired)
  1. Telepod has a bullet texture shown for items in flight (this effect is not desired, this type is MassDriverBolt)
  2. Telepod has a on recieve texture shown for recieving items from flight (this effect is possibly being caused by the bullet type MassDriverBolt, this effect is not desired)

# Acknowledgments

- Grav Belt textures are simply the vanilla Conveyor with the underlying dark gray coloration removed, also removed the "side" rails of the original.
- The Telepod base is a darkened and lightened version of the vanilla Mass Diver.
- The Telepad base is a darkened and lightened version of the vanilla Launch Pad.

